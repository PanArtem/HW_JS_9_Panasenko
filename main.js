const tabs = document.querySelector('.tabs'),
      tabsTitle = document.querySelectorAll('li.tabs-title'),
      tabsContent = document.querySelectorAll('li.tabs-content');

tabs.addEventListener("click", () => {

    tabsTitle.forEach((title) => {
        title.classList.remove('active');
    });

    let dataTabEvent = event.target.getAttribute('data-paragraph');

        tabsContent.forEach((p) => {
                if ( p.getAttribute('data-paragraph') === dataTabEvent ) {
                    event.target.classList.add('active');
                    p.classList.add('active-cont');

                } else {
                    p.classList.remove('active-cont');
            }
    });
});